import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { from, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { User } from 'src/app/shared/models/User';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private afStore: AngularFirestore) {}

  register(body: any) {
    return from(
      this.afStore.collection('users').add({
        name: body.name,
        surname: body.surname,
        password: body.password.password,
        mail: body.mail,
      })
    ).pipe(
      catchError(() => {
        throw new Error('Error al crear el usuario');
      })
    );
  }

  login(mail: string, password: string): Observable<User> {
    return <Observable<User>>this.afStore
      .collection('users', (ref) =>
        ref.where('mail', '==', mail).where('password', '==', password).limit(1)
      )
      .valueChanges()
      .pipe(
        map((res) => {
          if (res[0] != undefined) {
            let user: User = {
              name: (<User>res[0]).name,
              surname: (<User>res[0]).surname,
              mail: (<User>res[0]).mail,
            };
            return user;
          } else {
            throw new Error('Usuario no encontrado');
          }
        })
      );
  }
}
