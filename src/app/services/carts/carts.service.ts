import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, from, Observable, of } from 'rxjs';
import { catchError, concatMap, map, mergeMap, tap } from 'rxjs/operators';
import { Cart } from 'src/app/shared/models/Cart';
import { AppState } from 'src/app/store/app.state';
import { selectIdCart } from 'src/app/store/selectors/cart.selector';
import { selectUserMail } from 'src/app/store/selectors/user.selector';

@Injectable({
  providedIn: 'root',
})
export class CartsService {
  constructor(
    private afStore: AngularFirestore,
    private store: Store<AppState>
  ) {}

  /**
   * @return the user_id that is logged
   * */
  getUserId(): Observable<any> {
    return this.store.select(selectUserMail).pipe(
      concatMap((mail) => {
        return this.afStore
          .collection('users', (ref) => ref.where('mail', '==', mail).limit(1))
          .valueChanges({ idField: 'id' })
          .pipe(map((el) => el[0].id));
      })
    );
  }

  getCartId(): Observable<string | undefined> {
    return this.store.select(selectIdCart);
  }

  /**
   * @return the pending cart for the user, if there is not any, one is created and returned
   * */
  getProductCart() {
    return this.getUserId().pipe(
      concatMap((user_id) => {
        return this.afStore
          .collection('carts', (ref) =>
            ref.where('status', '==', 'pending').where('user_id', '==', user_id)
          )
          .valueChanges({ idField: 'id' })
          .pipe(
            concatMap((res) => {
              if (res.length != 0) {
                return of(res[0]);
              } else {
                return this.createCart(user_id);
              }
            }),
            catchError(() => {
              throw new Error('No pudo obtenerse el carrito');
            })
          );
      })
    );
  }

  /**
   * Creates a new pending cart for the user that is logged
   * @return the cart created
   * */
  createCart(user_id: string) {
    return from(
      this.afStore.collection('carts').add({
        user_id: user_id,
        status: 'pending',
      })
    ).pipe(
      concatMap((new_cart) => {
        return from(new_cart.get());
      }),
      catchError(() => {
        throw new Error('No pudo crearse un nuevo carrito');
      })
    );
  }

  /**
   * @param cart_id
   * @return an array of object in which the product and the quantity are specified
   * */
  getProductCarts(cart_id: string) {
    return this.afStore
      .collection('product_carts', (ref) => ref.where('cart_id', '==', cart_id))
      .valueChanges({ idField: 'id' })
      .pipe(
        concatMap((products_carts: any[]) => {
          if (products_carts.length == 0) {
            let array: any[] = [];
            return of(array);
          } else {
            return forkJoin(
              products_carts.map((el: any) => {
                return this.getProduct(el.product_id).pipe(
                  map((product) => {
                    return {
                      product: product,
                      quantity: el.quantity,
                      id: el.id,
                    };
                  })
                );
              })
            );
          }
        })
      );
  }

  /**
   * @param product_id
   * @return a Product object with all its data
   * */
  getProduct(product_id: string) {
    return this.afStore
      .doc('products/' + product_id)
      .get()
      .pipe(map((el) => el.data()));
  }

  getOldCarts(): Observable<Cart[]> {
    return <Observable<Cart[]>>this.getUserId().pipe(
      concatMap((userId) => {
        return <Observable<Cart[]>>this.afStore
        .collection('carts', (ref) => ref.where('status', '==', 'completed').where('user_id', '==', userId))
        .valueChanges()
      }),
      catchError(() => {
        throw new Error('No se pudo obtener el id del usuario')
      })
    )
  }

  addProduct(product_id: string, quantity: number) {
    return this.getCartId().pipe(
      concatMap((cart_id) => {
        return from(
          this.afStore.collection('product_carts').add({
            cart_id: cart_id,
            product_id: product_id,
            quantity: quantity,
          })
        ).pipe(
          concatMap((product) => from(product.get())),
          map((snapshot) => {
            let data = snapshot.data();
            return data;
          })
        );
      })
    );
  }

  editCart(productCartId: string, quantity: number) {
    return from(
      this.afStore.collection('product_carts').doc(productCartId).update({
        quantity: quantity,
      })
    );
  }

  removeCart(productCartId: string) {
    return from(
      this.afStore.collection('product_carts').doc(productCartId).delete()
    );
  }

  closeCart(productCartId: string, total: number) {
    return from(
      this.afStore.collection('carts').doc(productCartId).update({
        status: 'completed',
        total: total,
        date: new Date()
      })
    ).pipe(
      catchError(() => {
        throw new Error('No pudo cerrarse el carrito');
      })
    );
  }
}
