import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Product } from 'src/app/shared/models/Product';

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  productsList!: any[];

  constructor(private afStore: AngularFirestore) {}

  getProductsByLimit(limit: number): Observable<Product[]> {
    return <Observable<Product[]>>this.afStore
      .collection('products', (ref) => ref.limit(limit))
      .valueChanges({ idField: 'id' })
  }

  getProductsByKey(key: string): Observable<Product[]> {
    return <Observable<Product[]>>this.afStore
      .collection('products')
      .valueChanges({ idField: 'id' })
      .pipe(
        tap(() => console.log('entré acá')),
        map((products) => {
          return products.filter((el: any) => {
            let regex = new RegExp(key, 'ig');
            return regex.test(el.name);
          });
        })
      );
  }
}
