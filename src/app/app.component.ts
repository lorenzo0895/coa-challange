import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ProductsService } from './services/products/products.service';
import { getCart } from './store/actions/cart.actions';
import { AppState } from './store/app.state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'coa-challenge';
  
  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    this.store.dispatch(getCart())
  }
}
