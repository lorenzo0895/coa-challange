import { Component, EventEmitter, Output } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ICellRendererParams } from 'ag-grid-community';

@Component({
  selector: 'app-select-cell',
  templateUrl: './select-cell.component.html',
  styleUrls: ['./select-cell.component.scss'],
})
export class SelectCellComponent implements ICellRendererAngularComp {
  constructor() {}

  private params!: ICellRendererParams;
  value!: number;
  id!: string;

  agInit(params: ICellRendererParams): void {
    this.params = params;
    this.value = params.value.quantity;
    this.id = params.value.id;
  }
  refresh(params: ICellRendererParams): boolean {
    this.params = params;
    this.value = params.value.quantity;
    this.id = params.value.id;
    return true;
  }
  toggleChangeQuantity() {
    this.params.context.componentParent.changeQuantity(this.id, this.value);
  }
}
