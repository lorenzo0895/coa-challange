import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ICellRendererParams } from 'ag-grid-community';

@Component({
  selector: 'app-delete-cell',
  templateUrl: './delete-cell.component.html',
  styleUrls: ['./delete-cell.component.scss']
})
export class DeleteCellComponent implements ICellRendererAngularComp  {

  private params!: ICellRendererParams;
  private id!: string;

  constructor() { }
  agInit(params: ICellRendererParams): void {
    this.params = params;
    this.id = params.value;
  }
  refresh(params: ICellRendererParams): boolean {
    this.params = params;
    this.id = params.value;
    return true;
  }
  toggleChangeQuantity() {
    this.params.context.componentParent.deleteCart(this.id);
  }

}
