import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { getCart } from 'src/app/store/actions/cart.actions';
import { logout } from 'src/app/store/actions/user.actions';
import { AppState } from 'src/app/store/app.state';
import { selectCartProducts } from 'src/app/store/selectors/cart.selector';
import { selectIsLogged } from 'src/app/store/selectors/user.selector';
import { Product_Cart } from '../models/Product_Cart';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  isLooged$!: Observable<boolean>;
  products$!: Observable<Product_Cart[] | undefined>;
  quantity: number = 0!

  constructor(private store: Store<AppState>) {
    this.isLooged$ = this.store.select(selectIsLogged);
  }
  
  ngOnInit(): void {
    this.products$ = this.store.select(selectCartProducts);
    this.products$.subscribe(res => {
      this.quantity = res?.length ?? 0;
    })
  }

  logout() {
    this.store.dispatch(logout());
  }

}
