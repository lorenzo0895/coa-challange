import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ICellRendererParams } from 'ag-grid-community';

@Component({
  selector: 'app-image-cell',
  templateUrl: './image-cell.component.html',
  styleUrls: ['./image-cell.component.scss']
})
export class ImageCellComponent implements ICellRendererAngularComp {
  
  private params!: ICellRendererParams;
  url: string[] = [];
  
  agInit(params: ICellRendererParams): void {
    this.params = params;
    this.url = params.value;
  }
  refresh(params: ICellRendererParams): boolean {
    this.params = params;
    this.url = params.value;
    return true;
  }

}
