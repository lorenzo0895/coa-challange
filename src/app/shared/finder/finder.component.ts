import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-finder',
  templateUrl: './finder.component.html',
  styleUrls: ['./finder.component.scss'],
})
export class FinderComponent {
  form = new FormGroup({});
  model = { finder: null };
  fields: FormlyFieldConfig[] = [
    {
      key: 'finder',
      type: 'input',
      templateOptions: {
        label: 'Buscar',
        placeholder: 'Ej: excellent',
        appearance: 'outline',
      },
    },
  ];

  constructor(private router: Router) {}

  find(model: any) {
    this.router.navigateByUrl('/find/' + model.finder);
  }
}
