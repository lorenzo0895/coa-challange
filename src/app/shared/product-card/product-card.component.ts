import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Product } from 'src/app/shared/models/Product';
import { addProduct } from 'src/app/store/actions/cart.actions';
import { AppState } from 'src/app/store/app.state';
import { selectIsLogged } from 'src/app/store/selectors/user.selector';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {

  @Input() product!: Product;
  @Input() isMock: boolean = false;
  quantity: number = 1;
  isLogged$!: Observable<boolean | undefined>;

  constructor(private store: Store<AppState>, private router: Router) { }

  ngOnInit(): void {
    this.isLogged$ = this.store.select(selectIsLogged);
  }

  addOne() {
    this.quantity++;
  }
  
  removeOne() {
    this.quantity--;
  }

  addToCart() {
    this.isLogged$.subscribe(res => {
      if (res) {
        this.store.dispatch(addProduct({product_id: this.product.id, quantity: this.quantity}))
      } else {
        this.router.navigateByUrl('/login');
      }
    })
  }

}
