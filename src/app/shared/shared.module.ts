import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { NavbarComponent } from './navbar/navbar.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSelectModule } from '@angular/material/select';
import { CarouselComponent } from './carousel/carousel.component';
import { FooterComponent } from './footer/footer.component';
import { FinderComponent } from './finder/finder.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { MatBadgeModule } from '@angular/material/badge';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { SkeletonComponent } from './skeleton/skeleton.component';
import { ImageCellComponent } from './image-cell/image-cell.component';
import { AgGridModule } from 'ag-grid-angular';
import { SelectCellComponent } from './select-cell/select-cell.component';
import { DeleteCellComponent } from './delete-cell/delete-cell.component';
import { DialogComponent } from './dialog/dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import { ProductCardComponent } from './product-card/product-card.component';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [
    NavbarComponent,
    CarouselComponent,
    FooterComponent,
    FinderComponent,
    SkeletonComponent,
    ImageCellComponent,
    SelectCellComponent,
    DeleteCellComponent,
    DialogComponent,
    ProductCardComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatToolbarModule,
    MatSidenavModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    FormlyModule.forRoot({
      extras: { lazyRender: true },
      validationMessages: [
        { name: 'required', message: 'Este campo es requerido' },
      ],
    }),
    FormlyBootstrapModule,
    FormlyMaterialModule,
    MatAutocompleteModule,
    MatBadgeModule,
    AgGridModule.withComponents([ImageCellComponent, SelectCellComponent, DeleteCellComponent]),
    MatDialogModule,
    MatCardModule
  ],
  exports: [
    NavbarComponent,
    CarouselComponent,
    FooterComponent,
    FinderComponent,
    SkeletonComponent,
    ImageCellComponent,
    ProductCardComponent
  ],
})
export class SharedModule {}
