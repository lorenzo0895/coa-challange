import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-skeleton',
  templateUrl: './skeleton.component.html',
  styleUrls: ['./skeleton.component.scss']
})
export class SkeletonComponent implements OnInit {

  @Input() type: "circle" | "rectangle" = "circle";
  @Input() height: string = '50px';
  @Input() width: string = '50px';

  constructor() { }

  ngOnInit(): void {
  }

}
