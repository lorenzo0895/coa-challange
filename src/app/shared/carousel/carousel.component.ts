import { Component, Input, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit, OnDestroy {

  @Input() slides: string[] = [];
  position: number = 0;
  interval!: any;

  constructor() { }
  
  ngOnInit(): void {
    this.interval = setInterval(() => {
      if (this.position === this.slides.length - 1) {
        this.position = 0;
      } else {
        this.position = this.position + 1;
      }
    },5000)
  }

  ngOnDestroy(): void {
      this.interval = null;
  }

}
