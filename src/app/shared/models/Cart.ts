export interface Cart {
  id?: string;
  id_user?: string;
  status?: "pending" | "completed";
  total?: number;
  date?: any;
}