import { Product } from "./Product";

export interface Product_Cart {
  id?: string;
  id_product?: string;
  id_cart?: string;
  quantity?: number;
  product?: Product
}