export interface User {
  id?: string;
  name?: string;
  surname?: string;
  mail?: string;
  password?: string | null;
}