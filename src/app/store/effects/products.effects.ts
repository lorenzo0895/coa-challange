import { concatMap, switchMap, tap } from 'rxjs/operators';
import { catchError, map } from 'rxjs/operators';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { ProductsService } from 'src/app/services/products/products.service';
import * as actions from '../actions/products.actions';
import { of } from 'rxjs';

@Injectable()
export class ProductsEffects {
  getProducts$ = createEffect(() =>
    this.action$.pipe(
      ofType(actions.getProductsByKey),
      switchMap(({ key }) =>
        this.productsService.getProductsByKey(key).pipe(
          map((products) =>
            actions.getProductsByKeySuccess({ products: products })
          ),
          catchError(() => of(actions.errorProduct()))
        )
      )
    )
  );

  getProductsByLimit$ = createEffect(() =>
    this.action$.pipe(
      ofType(actions.getProductsByLimit),
      switchMap(({ limit }) =>
        this.productsService.getProductsByLimit(limit).pipe(
          map((products) =>
            actions.getProductsByLimitSuccess({ products: products })
          ),
          catchError(() => of(actions.errorProduct()))
        )
      )
    )
  );

  constructor(
    private action$: Actions,
    private productsService: ProductsService
  ) {}
}
