import { catchError, concatMap, map, switchMap, tap } from 'rxjs/operators';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { mergeMap } from 'rxjs/operators';
import * as actions from '../actions/user.actions';
import { of } from 'rxjs';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as cartActions from '../actions/cart.actions';

@Injectable()
export class UserEffects {
  login$ = createEffect(() =>
    this.action$.pipe(
      ofType(actions.login),
      switchMap(({ mail, password }) =>
        this.authService.login(mail, password).pipe(
          map((user) => actions.loginSuccess({ user: user })),
          tap(({user}) => {
            localStorage.setItem('user', JSON.stringify(user));
            window.location.href = '/';
          }),
          catchError((error) => {
            this._snackBar.open('Mail o contraseña incorrecta', 'Cerrar', {
              horizontalPosition: 'end',
              verticalPosition: 'top',
            });
            return of(actions.userError({ error: error }));
          })
        )
      )
    )
  );

  logout$ = createEffect(() =>
    this.action$.pipe(
      ofType(actions.logout),
      map(() => actions.logoutSuccess()),
      tap(() => {
        localStorage.removeItem('user');
        window.location.href = '/';
        this._snackBar.open('Se ha cerrado sesión', 'Cerrar', {
          horizontalPosition: 'end',
          verticalPosition: 'top',
        });
      }),
      catchError((error) => {
        return of(actions.userError({ error: error }));
      })
    )
  );

  register$ = createEffect(() =>
    this.action$.pipe(
      ofType(actions.register),
      switchMap((model) =>
        this.authService.register(model).pipe(
          map(() => actions.registerSuccess()),
          tap(() => {
            this.router.navigateByUrl('/login');
            this._snackBar.open('Se ha registrado con éxito', 'Cerrar', {
              horizontalPosition: 'end',
              verticalPosition: 'top',
            });
          }),
          catchError((error) => {
            this._snackBar.open('Error al registrar usuario', 'Cerrar', {
              horizontalPosition: 'end',
              verticalPosition: 'top',
            });
            return of(actions.userError({ error: error }));
          })
        )
      )
    )
  );

  constructor(
    private action$: Actions,
    private authService: AuthService,
    private router: Router,
    private _snackBar: MatSnackBar
  ) {}
}
