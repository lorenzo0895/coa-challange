import { catchError, concatMap, map, switchMap, tap } from 'rxjs/operators';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { CartsService } from 'src/app/services/carts/carts.service';
import * as actions from '../actions/cart.actions';
import { combineLatest, of } from 'rxjs';
import { Cart } from 'src/app/shared/models/Cart';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Injectable()
export class CartEffects {
  getCart$ = createEffect(() =>
    this.action$.pipe(
      ofType(actions.getCart),
      switchMap(() =>
        this.cartService.getProductCart().pipe(
          concatMap((cart: any) => {
            return combineLatest([
              of(cart),
              this.cartService.getProductCarts(cart.id),
            ]);
          }),
          map(([cart, products]: any) => {
            let cart1: Cart = {
              id: cart.id,
              status: cart.status,
            };
            return actions.getCartSuccess({ cart: cart1, products: products });
          }),
          catchError((error) => {
            return of(actions.cartError());
          })
        )
      )
    )
  );

  addProduct$ = createEffect(() =>
    this.action$.pipe(
      ofType(actions.addProduct),
      switchMap(({ product_id, quantity }) => {
        return this.cartService.addProduct(product_id, quantity).pipe(
          map((res: any) => {
            return actions.addProductSuccess({
              product: res.product,
              quantity: res.quantity,
            });
          }),
          tap(() => {
            this._snackBar.open('Producto añadido', 'Cerrar', {
              horizontalPosition: 'end',
              verticalPosition: 'top',
            });
          }),
          catchError(() => {
            this._snackBar.open('Error al añadir producto', 'Cerrar', {
              horizontalPosition: 'end',
              verticalPosition: 'top',
            });
            return of(actions.cartError());
          })
        );
      })
    )
  );

  editCart$ = createEffect(() =>
    this.action$.pipe(
      ofType(actions.editCart),
      switchMap(({ product_cart_id, quantity }) =>
        this.cartService.editCart(product_cart_id, quantity).pipe(
          map(() => {
            return actions.editCartSuccess({
              product_cart_id: product_cart_id,
              quantity: quantity,
            });
          }),
          tap(() => {
            this._snackBar.open('Cantidad modificada', 'Cerrar', {
              horizontalPosition: 'end',
              verticalPosition: 'top',
            });
          }),
          catchError(() => {
            this._snackBar.open('Error al modificar', 'Cerrar', {
              horizontalPosition: 'end',
              verticalPosition: 'top',
            });
            return of(actions.cartError());
          })
        )
      )
    )
  );

  deleteCart$ = createEffect(() =>
    this.action$.pipe(
      ofType(actions.removeProduct),
      switchMap(({ product_cart_id }) =>
        this.cartService.removeCart(product_cart_id).pipe(
          map(() => {
            return actions.removeProductSuccess({
              product_cart_id: product_cart_id,
            });
          }),
          tap(() => {
            this._snackBar.open('Producto eliminado', 'Cerrar', {
              horizontalPosition: 'end',
              verticalPosition: 'top',
            });
          }),
          catchError(() => {
            this._snackBar.open('Error al eliminar producto', 'Cerrar', {
              horizontalPosition: 'end',
              verticalPosition: 'top',
            });
            return of(actions.cartError());
          })
        )
      )
    )
  );

  getOldCarts$ = createEffect(() => 
    this.action$.pipe(
      ofType(actions.getOldCarts),
      switchMap(() => this.cartService.getOldCarts().pipe(
        map((oldCarts) => actions.getOldCartsSuccess({oldCarts: oldCarts}))
      ))
    )
  )

  closeCart$ = createEffect(() =>
    this.action$.pipe(
      ofType(actions.closeCart),
      switchMap(({ product_cart_id, total }) =>
        this.cartService.closeCart(product_cart_id, total).pipe(
          map(actions.closeCartSuccess),
          tap(() => {
            this._snackBar.open('Carrito confirmado', 'Cerrar', {
              horizontalPosition: 'end',
              verticalPosition: 'top',
            });
          }),
          tap(() => {
            window.location.href = '/';
          }),
          catchError(() => {
            this._snackBar.open('Error al confirmar el carrito', 'Cerrar', {
              horizontalPosition: 'end',
              verticalPosition: 'top',
            });
            return of(actions.cartError);
          })
        )
      )
    )
  );

  constructor(
    private action$: Actions,
    private cartService: CartsService,
    private _snackBar: MatSnackBar,
    private router: Router
  ) {}
}
