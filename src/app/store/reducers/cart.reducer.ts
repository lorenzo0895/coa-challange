import * as actions from '../actions/cart.actions';
import { createReducer, on } from '@ngrx/store';
import { CartState } from '../states/cart.state';

export const initialState: CartState = {
  isLoading: false,
  products: [],
};

export const cartReducer = createReducer(
  initialState,
  on(actions.initialValue, (state) => {
    return initialState;
  }),
  on(actions.getCart, (state) => {
    return (state = { ...state, isLoading: true });
  }),
  on(actions.getCartSuccess, (state, { cart, products }) => {
    return (state = {
      ...state,
      isLoading: false,
      cart: cart,
      products: products,
    });
  }),
  on(actions.addProduct, (state) => {
    return (state = { ...state, isLoading: true });
  }),
  on(actions.addProductSuccess, (state, { product, quantity }) => {
    return (state = {
      ...state,
      isLoading: false,
      products: [
        ...(state.products ?? []),
        { product: product, quantity: quantity },
      ],
    });
  }),
  on(actions.editCart, (state) => {
    return { ...state, isLoading: true };
  }),
  on(actions.editCartSuccess, (state, { product_cart_id, quantity }) => {
    return {
      ...state,
      isLoading: false,
      products: state.products?.map((el) => {
        if (el.id == product_cart_id) {
          return {...el, quantity: quantity};
        } else {
          return el;
        }
      }),
    }
  }),
  on(actions.removeProduct, (state) => {
    return (state = { ...state, isLoading: true });
  }),
  on(actions.removeProductSuccess, (state, { product_cart_id }) => {
    return (state = {
      ...state,
      isLoading: false,
      products: state.products?.filter((el) => el.id != product_cart_id),
    });
  }),

  on(actions.closeCart, (state) => {
    return {...state, isLoading: true}
  }),
  on(actions.closeCartSuccess, (state) => {
    return initialState
  }),

  on(actions.getOldCarts, (state) => {
    return {...state, isLoading: true}
  }),
  on(actions.getOldCartsSuccess, (state, {oldCarts}) => {
    return {...state, isLoading: false, oldCarts: oldCarts}
  }),

  on(actions.cartError, (state) => {
    return (state = { ...state, isLoading: false });
  })
);
