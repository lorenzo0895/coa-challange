import * as actions from '../actions/user.actions'
import { createReducer, on } from '@ngrx/store';
import { UserState } from '../states/user.state';

export const initialState: UserState = {
  isLoading: false,
  isLogged: false
};

export const userReducer = createReducer(
    initialState,
    on(actions.login, (state) => {
      return state = {...state, isLoading: true}
    }),
    on(actions.loginSuccess, (state, { user }) => {
      return state = {...state, user: user, isLoading: false, isLogged: true}
    }),
    on(actions.logout, (state) => {
      return state = {...state, isLoading: true}
    }),
    on(actions.logoutSuccess, (state) => {
      return state = initialState;
    }),
    on(actions.register, (state) => {
      return state = {...state, isLoading: true}
    }),
    on(actions.registerSuccess, (state) => {
      return state = {...state, isLoading: false};
    }),
    on(actions.userError, (state, { error }) => {
      return state = {...state, isLoading: false, error: error}
    })
)