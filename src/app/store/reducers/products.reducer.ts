import * as actions from '../actions/products.actions'
import { createReducer, on } from '@ngrx/store';
import { ProductsState } from '../states/products.state';

export const initialState: ProductsState = {
  isLoading: false,
  products: []
};

export const productsReducer = createReducer(
    initialState,
    on(actions.getProductsByKey, (state) => {
      return {...state, isLoading: true}
    }),
    on(actions.getProductsByKeySuccess, (state, { products }) => {
      return {...state, isLoading: false, products: products}
    }),

    on(actions.getProductsByLimit, (state) => {
      return {...state, isLoading: true}
    }),
    on(actions.getProductsByLimitSuccess, (state, { products }) => {
      return {...state, products: products, isLoading: false}
    }),

    on(actions.errorProduct, (state) => {
      return {...state, isLoading: false}
    }),
    
)