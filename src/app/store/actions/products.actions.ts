import { createAction, props } from '@ngrx/store';
import { Product } from 'src/app/shared/models/Product';

export const getProductsByKey = createAction(
  '[Products] Get Products',
  props<{ key: string }>()
);
export const getProductsByKeySuccess = createAction(
  '[Products] Get Products Success',
  props<{ products: Product[] }>()
);

export const getProductsByLimit = createAction(
  '[Products] Get Products By Limit',
  props<{ limit: number }>()
);
export const getProductsByLimitSuccess = createAction(
  '[Products] Get Products By Limit Success',
  props<{ products: Product[] }>()
);

export const errorProduct = createAction('[Products] Error');
