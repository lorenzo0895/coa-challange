import { createAction, props } from '@ngrx/store';
import { Cart } from 'src/app/shared/models/Cart';
import { Product } from 'src/app/shared/models/Product';
import { Product_Cart } from 'src/app/shared/models/Product_Cart';

export const initialValue = createAction('[Cart] Initial Value');

export const getOldCarts = createAction('[Cart] Get Old Carts');
export const getOldCartsSuccess = createAction(
  '[Cart] Get Old Carts Success',
  props<{ oldCarts: Cart[] }>()
);

export const getCart = createAction('[Cart] Get Cart');
export const getCartSuccess = createAction(
  '[Cart] Get Cart Success',
  props<{ cart: Cart; products: Product_Cart[] }>()
);
export const addProduct = createAction(
  '[Cart] Add Product',
  props<{ product_id: string; quantity: number }>()
);
export const addProductSuccess = createAction(
  '[Cart] Add Product Success',
  props<{ product: Product; quantity: number }>()
);
export const editCart = createAction(
  '[Cart] Edit Cart',
  props<{ product_cart_id: string; quantity: number }>()
);
export const editCartSuccess = createAction(
  '[Cart] Edit Cart Success',
  props<{ product_cart_id: string; quantity: number }>()
);
export const removeProduct = createAction(
  '[Cart] Remove Product',
  props<{ product_cart_id: string }>()
);
export const removeProductSuccess = createAction(
  '[Cart] Remove Product Success',
  props<{ product_cart_id: string }>()
);
export const closeCart = createAction(
  '[Cart] Cloce Cart',
  props<{ product_cart_id: string, total: number }>()
);
export const closeCartSuccess = createAction('[Cart] Cloce Cart Success');
export const cartError = createAction('[Cart] Cart Error');
