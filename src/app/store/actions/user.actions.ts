import { createAction, props } from '@ngrx/store';
import { User } from 'src/app/shared/models/User';

export const login = createAction(
  '[User] Get User',
  props<{ mail: string; password: string }>()
);
export const loginSuccess = createAction(
  '[User] Get User Success',
  props<{ user: User }>()
);

export const logout = createAction('[User] Logout');
export const logoutSuccess = createAction('[User] Logout Success');

export const register = createAction(
  '[User] Register',
  props<{
    name: string;
    surname: string;
    password1: string;
    password2: string;
    mail: string;
  }>()
);
export const registerSuccess = createAction('[User] Register Success');

export const userError = createAction(
  '[User] User Error',
  props<{ error: string }>()
);
