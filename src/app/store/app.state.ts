import { ActionReducer, ActionReducerMap, MetaReducer } from '@ngrx/store';
import { getCart } from './actions/cart.actions';
import { cartReducer } from './reducers/cart.reducer';
import { productsReducer } from './reducers/products.reducer';
import { userReducer } from './reducers/user.reducer';
import { CartState } from './states/cart.state';
import { ProductsState } from './states/products.state';
import { UserState } from './states/user.state';

export interface AppState {
  products: ProductsState;
  user: UserState;
  cart: CartState;
}

export const persistUser = (
  reducer: ActionReducer<any>
): ActionReducer<any> => {
  return (state, action) => {
    if (state?.user.isLogged === false) {
      let localStorageData = localStorage.getItem('user');
      if (localStorageData) {
        return reducer(
          {
            ...state,
            user: {
              user: JSON.parse(localStorageData),
              isLogged: true,
              isLoading: false,
            },
          },
          action
        );
      }
    }
    return reducer(state, action);
  };
};

// export const getCartMeta = (reducer: ActionReducer<any>): ActionReducer<any> => {
//   return (state, action) => {
//     if (!state?.cart.cart) {
//       return reducer({ ...state }, getCart());
//     } else {
//       return reducer(state, action);
//     }
//   };
// };

export const REDUCERS: ActionReducerMap<AppState> = {
  products: productsReducer,
  user: userReducer,
  cart: cartReducer,
};

export const METAREDUCERS: MetaReducer<any>[] = [persistUser];
