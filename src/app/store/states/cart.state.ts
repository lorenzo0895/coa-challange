import { Cart } from "src/app/shared/models/Cart";
import { Product_Cart } from "src/app/shared/models/Product_Cart";

export interface CartState {
  cart?: Cart;
  isLoading?: boolean;
  products?: Product_Cart[];
  error?: string;
  oldCarts?: Cart[];
}