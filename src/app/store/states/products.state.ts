import { Product } from "src/app/shared/models/Product";

export interface ProductsState {
  isLoading: boolean;
  products: Product[];
  error?: string;
}