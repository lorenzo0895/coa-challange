import { User } from "src/app/shared/models/User";

export interface UserState {
  user?: User;
  isLogged: boolean;
  isLoading?: boolean;
  error?: string;
}