import { AppState } from '../app.state';
import { createSelector } from '@ngrx/store';
import { UserState } from '../states/user.state';

export const selectUserFeature = (state: AppState) => state.user;

export const selectUser = createSelector(
  selectUserFeature,
  (state: UserState) => state.user
);
export const selectUserMail = createSelector(
  selectUserFeature,
  (state: UserState) => state.user?.mail
);
export const selectIsLogged = createSelector(
  selectUserFeature,
  (state: UserState) => state.isLogged
)
export const selectIsLoading = createSelector(
  selectUserFeature,
  (state: UserState) => state.isLoading
)
