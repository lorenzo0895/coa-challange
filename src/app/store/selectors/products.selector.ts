import { AppState } from './../app.state';
import { createSelector } from '@ngrx/store';
import { ProductsState } from '../states/products.state';

export const selectProductsFeature = (state: AppState) => state.products;

export const selectProducts = createSelector(
  selectProductsFeature,
  (state: ProductsState) => state.products
);
export const selectIsLoading = createSelector(
  selectProductsFeature,
  (state: ProductsState) => state.isLoading
);
