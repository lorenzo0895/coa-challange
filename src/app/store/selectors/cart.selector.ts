import { AppState } from '../app.state';
import { createSelector } from '@ngrx/store';
import { CartState } from '../states/cart.state';

export const selectCartFeature = (state: AppState) => state.cart;

export const selectCart = createSelector(
  selectCartFeature,
  (state: CartState) => state.cart
);
export const selectCartProducts = createSelector(
  selectCartFeature,
  (state: CartState) => state.products
);
export const selectIsLoading = createSelector(
  selectCartFeature,
  (state: CartState) => state.isLoading
);
export const selectIdCart = createSelector(
  selectCartFeature,
  (state: CartState) => state.cart?.id
)
export const selectOldCarts = createSelector(
  selectCartFeature,
  (state: CartState) => state.oldCarts
)
