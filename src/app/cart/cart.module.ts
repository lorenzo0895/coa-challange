import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CartRoutingModule } from './cart-routing.module';
import { SharedModule } from '../shared/shared.module';

import { AgGridModule } from 'ag-grid-angular';
import { CartComponent } from './pages/cart/cart.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { CloseCartsComponent } from './pages/close-carts/close-carts.component';

@NgModule({
  declarations: [CartComponent, CloseCartsComponent],
  imports: [
    CommonModule,
    CartRoutingModule,
    SharedModule,
    AgGridModule.withComponents([]),
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatProgressBarModule,
    MatDialogModule,
  ],
})
export class CartModule {}
