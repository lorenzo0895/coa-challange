import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CloseCartsComponent } from './close-carts.component';

describe('CloseCartsComponent', () => {
  let component: CloseCartsComponent;
  let fixture: ComponentFixture<CloseCartsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CloseCartsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CloseCartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
