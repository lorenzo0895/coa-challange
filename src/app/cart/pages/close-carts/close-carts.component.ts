import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ColDef } from 'ag-grid-community';
import { Observable } from 'rxjs';
import { Cart } from 'src/app/shared/models/Cart';
import { getOldCarts } from 'src/app/store/actions/cart.actions';
import { AppState } from 'src/app/store/app.state';
import { selectOldCarts } from 'src/app/store/selectors/cart.selector';
import { currencyFormatter } from '../../customFormaters';

@Component({
  selector: 'app-close-carts',
  templateUrl: './close-carts.component.html',
  styleUrls: ['./close-carts.component.scss']
})
export class CloseCartsComponent implements OnInit {

  oldCarts$!: Observable<Cart[] | undefined>;
  rowData: any[] = [];
  columnDefs: ColDef[] = [
    {
      headerName: '',
      field: 'index',
      width: 50,
      autoHeight: true,
    },
    {
      headerName: 'Fecha',
      field: 'fecha',
      width: 150,
      autoHeight: true,
      valueFormatter: (params) => {
        return params.data.fecha.toISOString().substring(0,10)
      }
    },
    {
      headerName: 'Total',
      field: 'total',
      width: 150,
      autoHeight: true,
      valueFormatter: (params) => currencyFormatter(params),
    },
  ]
  
  constructor(private store: Store<AppState>) {
  }

  ngOnInit(): void {
    this.oldCarts$ = this.store.select(selectOldCarts);
    this.oldCarts$.subscribe(carts => {
      if (carts != undefined) {
        this.setRow(carts);
      }
    })
    this.store.dispatch(getOldCarts());
  }

  setRow(carts: Cart[]) {
    this.rowData = carts.map((cart, index) => {
      return {
          index: index + 1,
          fecha: new Date(cart?.date?.seconds * 1000),
          total: cart.total
        }
      }
    )
  }
}
