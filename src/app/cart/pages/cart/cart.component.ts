import { Component, HostListener, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  CellClassParams,
  ColDef,
  ICellRendererParams,
  RowSpanParams,
} from 'ag-grid-community';
import { Observable } from 'rxjs';
import { DeleteCellComponent } from 'src/app/shared/delete-cell/delete-cell.component';
import { ImageCellComponent } from 'src/app/shared/image-cell/image-cell.component';
import { Product_Cart } from 'src/app/shared/models/Product_Cart';
import { SelectCellComponent } from 'src/app/shared/select-cell/select-cell.component';
import { closeCart, editCart, removeProduct } from 'src/app/store/actions/cart.actions';
import { AppState } from 'src/app/store/app.state';
import {
  selectCartProducts,
  selectIdCart,
  selectIsLoading,
} from 'src/app/store/selectors/cart.selector';
import {
  MatDialog,
} from '@angular/material/dialog';
import { DialogComponent } from 'src/app/shared/dialog/dialog.component';
import { currencyFormatter } from '../../customFormaters';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {
  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    if (event.target.innerWidth >= 820) {
      this.columnDefs = this.columnDefsWeb;
    } else {
      this.columnDefs = this.columnDefsMobile;
    }
  }
  public context: any;
  isLoading$!: Observable<boolean | undefined>;
  total: number = 0;
  products$!: Observable<Product_Cart[] | undefined>;
  idCart$!: Observable<string | undefined>;
  idCart!: string | undefined;
  defaultColDef: ColDef = {
    resizable: true,
  };
  columnDefsWeb: ColDef[] = [
    {
      headerName: 'Precio',
      field: 'column1',
      width: 120,
      autoHeight: true,
      colSpan: (params) => {
        if (isNaN(params.data.column1)) {
          return 2;
        } else {
          return 1;
        }
      },
      cellClassRules: {
        'is-number': (params) => !isNaN(params.value),
      },
      valueFormatter: (params) => {
        if (isNaN(params.data.column1)) {
          return params.data.column1;
        } else {
          return currencyFormatter(params);
        }
      },
    },
    {
      headerName: 'Cant.',
      field: 'column2',
      width: 90,
      autoHeight: true,
      cellRendererSelector: (params: ICellRendererParams) => {
        if (params.data.column2) {
          return {
            component: SelectCellComponent,
          };
        } else {
          return {
            component: null,
          };
        }
      },
    },
    {
      headerName: 'Total',
      field: 'column3',
      width: 120,
      autoHeight: true,
      type: 'numericColumn',
      cellRendererSelector: (params: ICellRendererParams) => {
        if (isNaN(params.data.column3)) {
          return {
            component: DeleteCellComponent,
          };
        } else {
          return {
            component: null,
          };
        }
      },
      valueFormatter: (params) => {
        if (isNaN(params.data.column1)) {
          return params.data.column1;
        } else {
          return currencyFormatter(params);
        }
      },
      cellClassRules: {
        'has-trash': (params: CellClassParams) => isNaN(params.value),
      },
    },
    {
      headerName: '',
      field: 'column4',
      width: 150,
      autoHeight: true,
      cellRendererSelector: (params: ICellRendererParams) => {
        if (params.data.column4) {
          return {
            component: ImageCellComponent,
          };
        } else {
          return {
            component: null,
          };
        }
      },
      rowSpan: (params: RowSpanParams) => {
        if (params.data.column4) {
          return 2;
        } else {
          return 1;
        }
      },
      cellClassRules: {
        'cell-span': "value!=''",
      },
      hide: false,
    },
  ];
  columnDefsMobile: ColDef[] = [
    {
      headerName: 'Precio',
      field: 'column1',
      width: 120,
      autoHeight: true,
      colSpan: (params) => {
        if (isNaN(params.data.column1)) {
          return 2;
        } else {
          return 1;
        }
      },
      cellClassRules: {
        'is-number': (params) => !isNaN(params.value),
      },
      valueFormatter: (params) => {
        if (isNaN(params.data.column1)) {
          return params.data.column1;
        } else {
          return currencyFormatter(params);
        }
      },
    },
    {
      headerName: 'Cant.',
      field: 'column2',
      width: 90,
      autoHeight: true,
      cellRendererSelector: (params: ICellRendererParams) => {
        if (params.data.column2) {
          return {
            component: SelectCellComponent,
          };
        } else {
          return {
            component: null,
          };
        }
      },
    },
    {
      headerName: 'Total',
      field: 'column3',
      width: 110,
      autoHeight: true,
      type: 'numericColumn',
      valueFormatter: (params) => {
        if (isNaN(params.data.column1)) {
          return params.data.column1;
        } else {
          return currencyFormatter(params);
        }
      },
      cellRendererSelector: (params: ICellRendererParams) => {
        if (isNaN(params.data.column3)) {
          return {
            component: DeleteCellComponent,
          };
        } else {
          return {
            component: null,
          };
        }
      },
      cellClassRules: {
        'has-trash': (params: CellClassParams) => isNaN(params.value),
      },
    },
    {
      headerName: '',
      field: 'column4',
      width: 150,
      autoHeight: true,
      cellRendererSelector: (params: ICellRendererParams) => {
        if (params.data.column4) {
          return {
            component: ImageCellComponent,
          };
        } else {
          return {
            component: null,
          };
        }
      },
      rowSpan: (params: RowSpanParams) => {
        if (params.data.column4) {
          return 2;
        } else {
          return 1;
        }
      },
      cellClassRules: {
        'cell-span': "value!=''",
      },
      hide: true,
    },
  ];
  columnDefs: ColDef[] = this.columnDefsMobile;
  rowData!: any | null;

  constructor(private store: Store<AppState>, public dialog: MatDialog) {
    this.context = { componentParent: this };
    this.idCart$ = this.store.select(selectIdCart);
    this.idCart$.subscribe(res => {
      this.idCart = res;
    })
  }

  ngOnInit(): void {
    if (window.innerWidth >= 820) {
      this.columnDefs = this.columnDefsWeb;
    }
    this.isLoading$ = this.store.select(selectIsLoading);
    this.products$ = this.store.select(selectCartProducts);
    this.products$.subscribe((res) => {
      this.setRow(res);
    });
  }

  setRow(productCarts: Product_Cart[] = []) {
    let array: any[] = [];
    this.total = 0;
    productCarts.forEach((el) => {
      this.total += (el.product?.price ?? 0) * (el.quantity ?? 0);
      array.push({
        column1: el.product?.name ?? '',
        column2: '',
        column3: el.id ?? '',
        column4: el.product?.image_url ?? '',
      });
      array.push({
        column1: el.product?.price,
        column2: { quantity: el.quantity, id: el.id },
        column3: (el.quantity ?? 0) * (el.product?.price ?? 0),
        column4: '',
      });
    });
    this.rowData = array;
  }

  changeQuantity(productCartId: string, quantity: number) {
    this.store.dispatch(
      editCart({ product_cart_id: productCartId, quantity: quantity })
    );
  }

  deleteCart(productCartId: string) {
    this.store.dispatch(removeProduct({ product_cart_id: productCartId }));
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '350px'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && this.idCart != undefined) {
        this.store.dispatch(closeCart({product_cart_id: this.idCart, total: this.total}))
      }
    });
  }

}
