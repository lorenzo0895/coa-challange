import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CartComponent } from './pages/cart/cart.component';
import { CloseCartsComponent } from './pages/close-carts/close-carts.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'current',
        component: CartComponent
      },
      {
        path: 'closed',
        component: CloseCartsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CartRoutingModule { }
