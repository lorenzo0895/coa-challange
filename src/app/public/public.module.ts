import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRoutingModule } from './public-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { FormlyMaterialModule } from '@ngx-formly/material';
import * as customValidators from './custom.validators';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { FindComponent } from './pages/find/find.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { ProductsGridComponent } from './components/products-grid/products-grid.component';
import { RegisterComponent } from './pages/register/register.component';
import { InfoComponent } from './components/info/info.component';

@NgModule({
  declarations: [HomeComponent, LoginComponent, RegisterComponent, ProductsGridComponent, FindComponent, InfoComponent],
  imports: [
    CommonModule,
    PublicRoutingModule,
    ReactiveFormsModule,
    FormlyModule.forRoot({
      extras: { lazyRender: true },
      validators: [{ name: 'email', validation: customValidators.email }],
      validationMessages: [
        { name: 'required', message: 'Este campo es requerido' },
        { name: 'minlength', message: customValidators.minlengthValidationMessage },
        { name: 'email', message: 'Debe ingresar un email válido' },
      ],
    }),
    FormlyBootstrapModule,
    FormlyMaterialModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    SharedModule,
    MatProgressBarModule
  ],
})
export class PublicModule {}
