import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Product } from 'src/app/shared/models/Product';
import { AppState } from 'src/app/store/app.state';
import { Store } from '@ngrx/store';
import { selectIsLoading, selectProducts } from 'src/app/store/selectors/products.selector';
import { getProductsByKey } from 'src/app/store/actions/products.actions';

@Component({
  selector: 'app-products-grid',
  templateUrl: './products-grid.component.html',
  styleUrls: ['./products-grid.component.scss'],
})
export class ProductsGridComponent implements OnInit {
  products!: Product[];
  products$!: Observable<any>;
  isLoading$!: Observable<boolean>;
  isLoading: boolean = true;

  constructor(
    private store: Store<AppState>
  ) {}

  ngOnInit(): void {
    this.isLoading$ = this.store.select(selectIsLoading);
    this.isLoading$.subscribe(res => {
      this.isLoading = res;
    })
    this.products$ = this.store.select(selectProducts);
    this.products$.subscribe((res) => {
      this.products = res;
    });
  }
}
