import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { getProductsByKey } from 'src/app/store/actions/products.actions';
import { AppState } from 'src/app/store/app.state';
import { selectIsLoading } from 'src/app/store/selectors/cart.selector';

@Component({
  selector: 'app-find',
  templateUrl: './find.component.html',
  styleUrls: ['./find.component.scss'],
})
export class FindComponent implements OnInit {
  isLoading$!: Observable<boolean | undefined>;

  constructor(
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.isLoading$ = this.store.select(selectIsLoading);
    this.activatedRoute.params.subscribe((params) => {
      this.store.dispatch(getProductsByKey({ key: params.key }));
    });
  }

}
