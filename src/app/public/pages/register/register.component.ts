import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { Observable } from 'rxjs';
import { register } from 'src/app/store/actions/user.actions';
import { AppState } from 'src/app/store/app.state';
import { selectIsLoading } from 'src/app/store/selectors/user.selector';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  isLoading$!: Observable<boolean | undefined>;
  form = new FormGroup({});
  model = {
    name: null,
    surname: null,
    mail: null,
    password: null,
  };
  fields: FormlyFieldConfig[] = [
    {
      key: 'name',
      type: 'input',
      templateOptions: {
        label: 'Nombre',
        placeholder: 'Nombre',
        appearance: 'outline',
        required: true,
      },
    },
    {
      key: 'surname',
      type: 'input',
      templateOptions: {
        label: 'Apellido',
        placeholder: 'Apellido',
        appearance: 'outline',
        required: true,
      },
    },
    {
      key: 'mail',
      type: 'input',
      templateOptions: {
        type: 'email',
        label: 'Mail',
        placeholder: 'Mail',
        appearance: 'outline',
        required: true,
      },
      validators: {
        validation: ['email'],
      }
    },
    {
      key: 'password',
      validators: {
        fieldMatch: {
          expression: (control:any) => {
            const value = control.value;
            return value.passwordConfirm === value.password
              || (!value.passwordConfirm || !value.password);
          },
          message: 'Las contraseñas no coinciden',
          errorPath: 'passwordConfirm',
        },
      },
      fieldGroup: [
        {
          key: 'password',
          type: 'input',
          templateOptions: {
            type: 'password',
            label: 'Contraseña',
            placeholder: 'Contraseña',
            appearance: 'outline',
            required: true,
            minLength: 8,
          },
        },
        {
          key: 'passwordConfirm',
          type: 'input',
          templateOptions: {
            type: 'password',
            label: 'Confirme contraseña',
            placeholder: 'Confirme contraseña',
            appearance: 'outline',
            required: true,
          },
        },
      ]
    }
  ];

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.isLoading$ = this.store.select(selectIsLoading);
  }

  register(model: any) {
    if (this.form.invalid) {
      return;
    } else {
      this.store.dispatch(register(model))
    }
  }

}
