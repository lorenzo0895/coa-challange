import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { CartsService } from 'src/app/services/carts/carts.service';
import { getCart } from 'src/app/store/actions/cart.actions';
import { getProductsByLimit } from 'src/app/store/actions/products.actions';
import { AppState } from 'src/app/store/app.state';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  slides: string[] = [
    '../../assets/slide1.jpg',
    '../../assets/slide2.jpg',
  ]

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.store.dispatch(getProductsByLimit({limit: 5}))
  }
}
