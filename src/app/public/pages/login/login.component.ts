import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { Observable } from 'rxjs';
import { login } from 'src/app/store/actions/user.actions';
import { AppState } from 'src/app/store/app.state';
import { selectIsLoading } from 'src/app/store/selectors/user.selector';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  isLoading$!: Observable<boolean | undefined>;
  form = new FormGroup({});
  model = {
    mail: null,
    password: null,
  };
  fields: FormlyFieldConfig[] = [
    {
      key: 'mail',
      type: 'input',
      templateOptions: {
        type: 'email',
        label: 'Mail',
        placeholder: 'Mail',
        appearance: 'outline',
        required: true,
      },
      validators: {
        validation: ['email'],
      }
    },
    {
      key: 'password',
      type: 'input',
      templateOptions: {
        type: 'password',
        label: 'Contraseña',
        placeholder: 'Contraseña',
        appearance: 'outline',
        required: true,
        minLength: 8
      },
    },
  ];

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.isLoading$ = this.store.select(selectIsLoading);
  }

  login(model: any) {
    this.store.dispatch(login({mail: model.mail, password: model.password}))
  }
}
