import { AbstractControl } from "@angular/forms";

export function minlengthValidationMessage(err: any, field: any) {
  return `Debe contener al menos ${field.templateOptions.minLength} caracteres`;
}

export function email(control: AbstractControl): {[key: string]: any} | null {
  let regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (regex.test(control.value)){
    return null;
  } else {
    return {'email': true};
  }
}